package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"os/exec"
	"os/signal"
	"runtime/debug"
	"strings"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"
)

var debug_mode = false
var mock = false

func main() {
	// show line
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.BoolVar(&debug_mode, "d", false, "show debug log")
	flag.BoolVar(&mock, "m", false, "mock server response")

	flag.Parse()

	command := "_vpn"
	if mock {
		command = "_mockvpn"
	}
	setup_cleanup(command)
	cmd := exec.Command(command, "start")

	r, w := io.Pipe()
	r2, w2 := io.Pipe()

	stdin, err := cmd.StdinPipe()
	check(err)
	defer stdin.Close()
	cmd.Stderr = w2
	cmd.Stdout = w

	go interactWithProcess(r, stdin, "stdout")
	go interactWithProcess(r2, stdin, "stderr")

	cmd.Start()
	// gc is useless as the program will hang until
	// it is aborted
	disable_gc()
	cmd.Wait()
}

func interactWithProcess(stdoutErr io.Reader, stdin io.Writer, prefix string) {
	go respondToReader(stdoutErr, stdin, prefix)
}

// val on stdout => val on stderr
const PEM_INDIC = "Using private key file"       //PEM pass phrase:
const VPN_PASS_INDIC = "Content-Security-Policy" // Password
const OTP_INDIC = "Microsoft verification code"  //Response:

var passwdHasBeenProcessed = false

func respondToReader(r io.Reader, w io.Writer, prefix string) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		m := scanner.Text()
		println("[", prefix, "]", m, "------")
		switch {
		case strings.Contains(m, VPN_PASS_INDIC):
			// workaround because unable to read from stderr consistently
			if !passwdHasBeenProcessed {
				fmt.Println("please provide your vpn password:")
				w.Write(getPasswordFromUser())
				passwdHasBeenProcessed = true
				fmt.Println("password forwarded to auth agent ...")
			}
		case strings.Contains(m, PEM_INDIC):
			fmt.Println("retrieving pem")
			w.Write(getPem())
		case strings.Contains(m, OTP_INDIC):
			fmt.Println("responding to otp challenge")
			w.Write(getOtpPass())
		case strings.Contains(m, "Continuing in background"):
			fmt.Println(m)
			fmt.Println("Program will block until vpn is stopped")
		}
	}
}

func getOtpPass() []byte {
	secret := strings.TrimSuffix(string(getFromPass("otp/main")), "\n")
	cmd := exec.Command("oathtool", "--totp", "-b", "-d", "6", secret)
	var buf bytes.Buffer
	cmd.Stdout = &buf
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	// already contains \n
	return buf.Bytes()
}

func getFromPass(key string) []byte {
	cmd := exec.Command("pass", key)
	var buf bytes.Buffer
	cmd.Stdout = &buf
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		log.Fatal("failed to retrieve key pass ", key, ":", err)
	}
	//log.Println("buffer from getpass:|", buf.Bytes(), "|------")
	return buf.Bytes()
}

func getPem() []byte {
	return getFromPass("vpn/pin")
}

func getPasswordFromUser() []byte {
	// Input from user already includes a new line
	passwd, err := terminal.ReadPassword(int(os.Stdin.Fd()))
	check(err)
	// terminal strips the \n
	passwd = append(passwd, '\n')
	return passwd
}

func check(errs ...error) {
	for _, err := range errs {
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func stop_vpn(command string) {
	cmd := exec.Command(command, "stop")
	cmd.Start()
}

func setup_cleanup(command string) {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		stop_vpn(command)
		os.Exit(0)
	}()
}

func disable_gc() {
	debug.SetGCPercent(-1)
	debug.SetMemoryLimit(math.MaxInt64)
}

func println(msg ...string) {
	if debug_mode {
		for _, m := range msg {
			fmt.Print(m)
		}
		fmt.Println("")
	}
}
