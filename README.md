# Autogol

Tool to automatically authenticate to a vpn retrieving passwords from a "pass" password store.

Acts as an `expect` script reading stdout/stderr and responding on stdin.
